#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"

// Added by Adrian Colesa - VM
#ifdef VM
#include <stdint.h>
#include "filesys/file.h"
#include "vm/spte.h"
#endif

tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);

// Added by Adrian Colesa - VM
#ifdef VM
struct supl_pte *
page_lookup (const void *pg_no);
#endif

#endif /* userprog/process.h */
