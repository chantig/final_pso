/*
 * exitcodes.h
 *
 *  Created on: Nov 9, 2018
 *      Author: mga4clj
 */

#ifndef USERPROG_SYSINFO_H_
#define USERPROG_SYSINFO_H_

#define EXIT_NULL_ADDRESS -1
#define EXIT_NOT_USER_ADDRESS -1
#define EXIT_UNMAPPED_ADDRESS -1

#define INVALID_FILE -1

#define EXIT_ERROR -1

#define NO_ARGS_HALT 0
#define NO_ARGS_EXIT 1
#define NO_ARGS_EXEC 1
#define NO_ARGS_WAIT 1
#define NO_ARGS_CREATE 2
#define NO_ARGS_REMOVE 1
#define NO_ARGS_OPEN 1
#define NO_ARGS_FILESIZE 1
#define NO_ARGS_READ 3
#define NO_ARGS_WRITE 3
#define NO_ARGS_SEEK 2
#define NO_ARGS_TELL 1
#define NO_ARGS_CLOSE 1
#define NO_ARGS_MMAP 2
#define NO_ARGS_MUNMAP 1

#endif /* USERPROG_SYSINFO_H_ */
