#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "lib/kernel/console.h"
#include "devices/input.h"
#include "process.h"
#include "devices/timer.h"
#include "pagedir.h"
#include "threads/malloc.h"
#include "threads/vaddr.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "fdstruct.h"
#include "sysinfo.h"

#define MAX_PATH 512
#define STACK (f->esp)
#define ARGS_OFFSET 4
#define ARGS_ADDRESS(stack) (stack + ARGS_OFFSET)

static void syscall_handler(struct intr_frame *);

void sys_exit(int status);
inline static pid_t sys_exec(struct intr_frame *f);
inline static int sys_wait(struct intr_frame *f);
inline static bool sys_create(struct intr_frame *f);
inline static bool sys_remove(struct intr_frame *f);
inline static int sys_open(struct intr_frame *f);
inline static int sys_filesize(struct intr_frame *f);
inline static int sys_write(struct intr_frame *f);
inline static int sys_read(struct intr_frame *f);
inline static void sys_seek(struct intr_frame *f);
inline static unsigned sys_tell(struct intr_frame *f);
inline static void sys_close(struct intr_frame *f);
inline static int sys_ticks(struct intr_frame *f);

inline static void sys_read_args(const void *stack, int *buffer, int nr_args);
inline static void sys_validate_address(const void *);
inline static void sys_validate_stack_args(const void *stack, int nr_args);
inline static void sys_validate_sized_buffer(const void*, unsigned);
inline static void sys_validate_zero_buffer(const void*);

inline static int sys_mmap(struct intr_frame *f);
inline static void sys_munmap(struct intr_frame *f);
inline static void* sys_get_stack_size();

void syscall_init(void)
{
	sema_init(&filesys_lock, 1);
	intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void syscall_handler(struct intr_frame *f)
{
	//validate stack pointer
	sys_validate_address(STACK);
	int syscall_no = ((int*) STACK)[0];
	//printf("Syscall no %d\n", syscall_no);
	switch (syscall_no)
	{
	case SYS_HALT:
		break;
	case SYS_EXIT:
	{
		int args[NO_ARGS_EXIT];
		sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_EXIT);
		int status = args[0];
		sys_exit(status);
		break;
	}

	case SYS_EXEC:
	{
		f->eax = sys_exec(f);
		break;
	}
	case SYS_WAIT:
		f->eax = sys_wait(f);
		break;
	case SYS_CREATE:
		f->eax = sys_create(f);
		break;
	case SYS_REMOVE:
		f->eax = sys_remove(f);
		break;
	case SYS_OPEN:
		f->eax = sys_open(f);
		break;
	case SYS_FILESIZE:
		f->eax = sys_filesize(f);
		break;
	case SYS_READ:
		f->eax = sys_read(f);
		break;
	case SYS_WRITE:
		f->eax = sys_write(f);
		break;
	case SYS_SEEK:
		sys_seek(f);
		break;
	case SYS_TELL:
		f->eax = sys_tell(f);
		break;
	case SYS_CLOSE:
		sys_close(f);
		break;
	case SYS_TEST:
		printf("SYS_TEST system call!\n");
		break;
	case SYS_TICKS:
		printf("SYS_TICKS system call!\n");
		f->eax = sys_ticks(f);
		break;
	case SYS_MMAP:
		f->eax = sys_mmap(f);
		break;
	case SYS_MUNMAP:
		sys_munmap(f);
		break;
	default:
		;
	}
	return;
}

inline void sys_exit(int status)
{
	printf("%s: exit(%d)\n", thread_current()->name, status);

	struct child_process *pstatus = thread_current()->pstatus;
	if (!thread_is_orphan())
	{
		pstatus->exit_status = status;
		pstatus->thread = NULL;
		sema_up(&pstatus->waiting_lock);
	}
	thread_exit();
}

inline static pid_t sys_exec(struct intr_frame *f)
{
	int args[NO_ARGS_EXEC];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_EXEC);

	const void *filename = (char*) args[0];
	sys_validate_zero_buffer(filename);

	tid_t tid = process_execute(filename);

	struct child_process *pstatus = thread_get_child(tid);
	sema_down(&pstatus->loading_lock);
	return pstatus->tid;
}

inline static int sys_wait(struct intr_frame *f)
{
	int args[NO_ARGS_WAIT];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_WAIT);

	pid_t pid = args[0];
	return process_wait(pid);
}

inline static bool sys_create(struct intr_frame *f)
{
	int args[NO_ARGS_CREATE];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_CREATE);

	const char *fname = (char *) args[0];
	unsigned initial_size = args[1];
	sys_validate_zero_buffer(fname);

	sema_down(&filesys_lock);
	bool status = filesys_create(fname, initial_size);
	sema_up(&filesys_lock);

	return status;
}

inline static bool sys_remove(struct intr_frame *f)
{
	int args[NO_ARGS_REMOVE];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_REMOVE);

	const char *fname = (char *) args[0];
	sys_validate_zero_buffer(fname);

	sema_down(&filesys_lock);
	bool status = filesys_remove(fname);
	sema_up(&filesys_lock);

	return status;
}

inline static int sys_open(struct intr_frame *f)
{
	int args[NO_ARGS_OPEN];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_OPEN);

	const void *fname = (void *) args[0];
	sys_validate_zero_buffer(fname);

	sema_down(&filesys_lock);
	struct file *file = filesys_open(fname);
	sema_up(&filesys_lock);

	if (NULL == file)
		return INVALID_FILE;

	struct file_descriptor *fd = (struct file_descriptor*) malloc(
			sizeof(struct file_descriptor));
	struct thread *t = thread_current();
	fd->fd = t->file_no++;
	fd->file = file;

	list_push_back(&t->fdt, &fd->elem);

	return fd->fd;
}

inline static int sys_filesize(struct intr_frame *f)
{
	int args[NO_ARGS_FILESIZE];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_FILESIZE);

	int fd = args[0];
	struct file_descriptor *fde = thread_get_file(fd);
	if (NULL == fde)
		return INVALID_FILE;

	sema_down(&filesys_lock);
	int size = file_length(fde->file);
	sema_up(&filesys_lock);

	return size;
}

inline static int sys_read(struct intr_frame *f)
{
	int args[NO_ARGS_READ];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_READ);

	int fd = args[0];
	const void *buffer = (void *) args[1];
	unsigned size = args[2];

	if (size == 0)
		return 0;
	sys_validate_sized_buffer(buffer, size);

	unsigned read_bytes = 0;

	if (fd == STDIN_FILENO)
	{
		sema_down(&filesys_lock); //-------------------------------------------------------------------------????trebuie?
		while (read_bytes < size)
		{
			((uint8_t*) buffer)[read_bytes++] = input_getc();
		}
		sema_up(&filesys_lock);
	}
	else
	{
		struct file_descriptor *fde = thread_get_file(fd);
		if (fde == NULL)
			return INVALID_FILE;

		sema_down(&filesys_lock);
		read_bytes = file_read(fde->file, buffer, size);
		sema_up(&filesys_lock);
	}
	return read_bytes;
}

inline static int sys_write(struct intr_frame *f)
{
	int args[NO_ARGS_WRITE];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_WRITE);

	int fd = args[0];
	const void *buffer = (void *) args[1];
	unsigned size = args[2];

	if (size == 0)
		return 0;
	sys_validate_sized_buffer(buffer, size);

	int written_bytes = size;
	if (fd == STDOUT_FILENO)
	{
		putbuf(buffer, size);
	}
	else
	{
		struct file_descriptor *fde = thread_get_file(fd);
		if (fde == NULL)
			return INVALID_FILE;

		sema_down(&filesys_lock);
		written_bytes = file_write(fde->file, buffer, size);
		sema_up(&filesys_lock);
	}
	return written_bytes;
}

inline static void sys_seek(struct intr_frame *f)
{
	int args[NO_ARGS_SEEK];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_SEEK);
	int fd = args[0];
	unsigned position = args[1];

	struct file_descriptor *fde = thread_get_file(fd);
	if (NULL == fde)
		return;

	sema_down(&filesys_lock);
	file_seek(fde->file, position);
	sema_up(&filesys_lock);
}

inline static unsigned sys_tell(struct intr_frame *f)
{
	int args[NO_ARGS_TELL];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_TELL);
	int fd = args[0];

	struct file_descriptor *fde = thread_get_file(fd);
	if (NULL == fde)
		return INVALID_FILE;

	sema_down(&filesys_lock);
	unsigned pos = file_tell(fde->file);
	sema_up(&filesys_lock);

	return pos;
}

inline static void sys_close(struct intr_frame *f)
{
	int args[NO_ARGS_CLOSE];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_CLOSE);
	int fd = args[0];

	struct file_descriptor *fde = thread_get_file(fd);
	if (NULL == fde)
		return;

	sema_down(&filesys_lock);
	file_close(fde->file);
	sema_up(&filesys_lock);

	list_remove(&fde->elem);
	free(fde);
}

inline static int sys_ticks(struct intr_frame *f UNUSED)
{
	return timer_ticks();
}

inline static int sys_mmap(struct intr_frame *f)
{
	int args[NO_ARGS_MMAP];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_MMAP);

	int fd = args[0];
	void *mapping_addr = (void*) args[1];

	if (fd == 0 || fd == 1)
		return -1; //can not map STDIN or STDOUT

	if (mapping_addr == NULL || //page 0 is not mapped
			((int) mapping_addr << 20) != NULL || //misalign
			page_lookup((uint32_t) mapping_addr / PGSIZE) != NULL || //overlap
			mapping_addr >= sys_get_stack_size())
		return -1;

	struct file_descriptor *fde = thread_get_file(fd);
	if (fde == NULL)
		return INVALID_FILE;

	if (fde != NULL)
	{

		int fsize = file_length(fde->file);
		if (fsize == 0)
			return -1;

		int read_bytes = fsize;
		int read_bytes_last_page = fsize % PGSIZE;
		int zero_bytes = PGSIZE - read_bytes_last_page;

		int no_pages = read_bytes / PGSIZE;
		if (read_bytes_last_page != 0)
			no_pages += 1;
		//printf("Read bytes %d, zero_bytes %d\n", read_bytes, zero_bytes);

		uint8_t *upage = (uint8_t *) mapping_addr;
		off_t crt_ofs = 0;

		for (int page = 1; page < no_pages; page++)
		{
			if (page_lookup((uint32_t) (upage) / PGSIZE) != NULL || //overlap
					upage >= sys_get_stack_size())
			{
				return -1;
			}
			upage += PGSIZE;
		}

		upage = (uint8_t *) mapping_addr;
		file_seek(fde->file, 0);

		struct supl_pte *spte;
		struct fil *file = file_reopen(fde->file);

		struct mid *mid = (struct mid*) malloc(sizeof(struct mid));
		mid->id = thread_current()->mid_no++;
		mid->addr = mapping_addr;
		mid->size = fsize;
		mid->file = file;
		list_init(&mid->pages);

		while (read_bytes > 0 || zero_bytes > 0)
		{
			/* Calculate how to fill this page.
			 We will read PAGE_READ_BYTES bytes from FILE
			 and zero the final PAGE_ZERO_BYTES bytes. */
			size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
			size_t page_zero_bytes = PGSIZE - page_read_bytes;

			// Added by Adrian Colesa - VM
			spte = malloc(sizeof(struct supl_pte));
			spte->virt_page_addr = upage;
			spte->virt_page_no = ((unsigned int) upage) / PGSIZE;
			spte->ofs = crt_ofs;
			spte->page_read_bytes = page_read_bytes;
			spte->page_zero_bytes = page_zero_bytes;
			spte->writable = true;
			spte->executable = false;
			spte->file = file;

			hash_insert(&thread_current()->supl_pt, &spte->he);
			list_push_back(&mid->pages, &spte->elem);

			crt_ofs += page_read_bytes;

			/* Advance. */
			read_bytes -= page_read_bytes;
			zero_bytes -= page_zero_bytes;
			upage += PGSIZE;
		}

		list_push_back(&thread_current()->mids, &mid->elem);
		return mid->id;
	}
	else
	{
		return -1;
	}
}

inline static void sys_munmap(struct intr_frame *f)
{
	int args[NO_ARGS_MUNMAP];
	sys_read_args(ARGS_ADDRESS(STACK), args, NO_ARGS_MUNMAP);

	int id = args[0];

	struct mid* mid = thread_get_mfile(id);
	if (mid == NULL)
		return;
	process_remove_mapped_files(mid);
}

inline static void sys_read_args(const void *stack, int *args, int nr_args)
{
	sys_validate_stack_args(stack, nr_args);
	int *istack = (int *) stack;
	for (int i = 0; i < nr_args; i++)
		args[i] = istack[i];

}

inline static void sys_validate_address(const void *address)
{
	if ( NULL == address)
		sys_exit(EXIT_NULL_ADDRESS); /*address is null*/
	if (!is_user_vaddr(address))
		sys_exit(EXIT_NOT_USER_ADDRESS); /*not an user adddress*/
	if ( NULL == pagedir_get_page(thread_current()->pagedir, address) &&
	NULL == page_lookup(((uint32_t) address) / PGSIZE))
		sys_exit(EXIT_UNMAPPED_ADDRESS);

}

inline static void sys_validate_stack_args(const void *stack, int nr_args)
{
	int *istack = (int*) stack;
	for (int arg = 0; arg < nr_args; arg++)
		sys_validate_address(istack + arg);
}

inline static void sys_validate_sized_buffer(const void *buffer, unsigned size)
{
	char *cbuf = (char *) buffer;
	for (unsigned i = 0; i < size; i++)
		sys_validate_address(cbuf + i);
}

inline static void sys_validate_zero_buffer(const void* buffer)
{
	char *cbuf = (char*) buffer;
	int size = 0;
	while (size < MAX_PATH)
	{
		sys_validate_address(cbuf + size);
		if (cbuf[size++] == '\0')
			break;
	}
}

inline static void* sys_get_stack_size()
{
	return PHYS_BASE - thread_current()->stack_pages * PGSIZE;
}
