/*
 * my_userprog_invalid_pointer.c
 *
 *  Created on: Nov 4, 2018
 *      Author: mga4clj
 */

#include <stdio.h>
#include <syscall.h>

#define PHYS_BASE 0xc0000000     /* 3 GB. */

int
main (int argc, char **argv)
{
	int *p;
	// case 1
	//p = 0;		// NULL pointer

	// case 2
	p = PHYS_BASE + 1;	// pointer in the kernel space
	printf("main argc= %d\n", argc);
	for(int i = 0; i < argc; i++)
		printf("main argv[%d] = %s\n", i , argv[i]);
	//printf("argc %d argv %s\n", argc, argv[0]);
	// pointer usage
	//*p = 10;

	//write(1, 0x23144623, 2);

	return EXIT_SUCCESS;
}


