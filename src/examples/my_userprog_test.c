/*
 * my_userprog_test.c
 *
 *  Created on: Nov 4, 2018
 *      Author: mga4clj
 */

#include <stdio.h>
#include <syscall.h>

int
main (int argc, char **argv)
{
	char c;


	//halt();

	int pid_child = exec("child.exe");
	wait(pid_child);

	create("file", 10);
	remove("file");
	int fd = open("file");
	int size = filesize(fd);
	read(fd, &c, sizeof(c));
	write(fd, &c, sizeof(c));
	seek(fd, 0);
	int pos = tell(fd);
	close(fd);
	test();
	return EXIT_SUCCESS;
}


