/*
 * my_userprog_division_by_zero.c
 *
 *  Created on: Nov 4, 2018
 *      Author: mga4clj
 */

#include <stdio.h>
#include <syscall.h>

int
main (int argc, char **argv)
{
	int d;

	d=10/0;

	printf("d=%d\n", d);

	return EXIT_SUCCESS;
}


