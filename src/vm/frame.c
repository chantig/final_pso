#include "frame.h"
#include <bitmap.h>
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/vaddr.h"
#include "threads/thread.h"
#include <stdio.h>
#include <string.h>
#include "swap.h"
#include "spte.h"

static struct frame_entry * frame_table;
static void * user_frames = NULL;

#define FRAME_FREE 0
#define FRAME_USED 1
static struct bitmap *free_frames_bitmap;
static struct lock frame_in_lock;
static struct lock frame_out_lock;
static size_t no_of_pages;
void * prepare_frame(struct supl_pte* spte);
void frame_evict(size_t idx);
void *frame_swap_in(struct supl_pte* spte);
void set_last_accessed(int64_t last_time);
size_t rlu();

void frame_table_init(size_t number_of_user_pages)
{
	frame_table = malloc(number_of_user_pages * sizeof(struct frame_entry));
	if(NULL == frame_table)
	{
		PANIC("Unable to allocate space for the frame table.");
	}
	no_of_pages = number_of_user_pages;
	memset(frame_table, 0, number_of_user_pages * sizeof(struct frame_entry));

	user_frames = palloc_get_multiple(PAL_USER, number_of_user_pages);
	if(NULL == user_frames)
	{
		PANIC("Unable to claim user space for the frame manager.");
	}


	free_frames_bitmap = bitmap_create( number_of_user_pages);
	if(NULL == free_frames_bitmap)
	{
	    PANIC("Unable to initialize swap table!");
	}

	bitmap_set_all(free_frames_bitmap, FRAME_FREE);
	lock_init(&frame_in_lock);
	lock_init(&frame_out_lock);
}

void *frame_alloc( enum palloc_flags flags,  struct supl_pte* spte)
{
	ASSERT(0 != (flags & PAL_USER));
	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);

	char *free_frame = (char *)prepare_frame(spte);

    memset(free_frame, 0, PGSIZE);
    return free_frame;
}

void * prepare_frame(struct supl_pte* spte)
{
	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);

	size_t free_idx = 0;
    printf("%s\n", "frame_prepare");

	lock_acquire(&frame_in_lock);
	free_idx = bitmap_scan_and_flip(free_frames_bitmap, 0, 1, FRAME_FREE);

	if(BITMAP_ERROR == free_idx)
	{
		free_idx = rlu();
		frame_evict(free_idx);
	}

    frame_table[free_idx].last_time = timer_ticks();
	frame_table[free_idx].spte = spte;
	frame_table[free_idx].spte->frame_page_idx = free_idx;
    frame_table[free_idx].spte->swapped_out = false;
	lock_release(&frame_in_lock);
    frame_table[free_idx].ownner_thread = thread_current();
    printf("%s\n", "frame_prepare end");

	return (char *)user_frames + PGSIZE * free_idx;
}

void frame_evict(size_t idx)
{
	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);
    printf("%s\n", "frame_evict");

	if (pagedir_is_dirty(frame_table[idx].ownner_thread->pagedir, frame_table[idx].spte->virt_page_addr))
	{
		size_t swap_idx = swap_out(user_frames + PGSIZE * idx);
		frame_table[swap_idx].spte->swap_idx = swap_idx;
		frame_table[swap_idx].spte->swapped_out = true;
	}
	pagedir_clear_page(frame_table[idx].ownner_thread->pagedir, frame_table[idx].spte->virt_page_addr);
    printf("%s\n", "frame_evit end");

}

void *frame_swap_in(struct supl_pte* spte)
{
    printf("%s\n", "frame_swap in");

	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);
	char *frame_to_write = (char *)prepare_frame(spte);
	swap_in(spte->swap_idx, frame_to_write);
    printf("%s\n", "frame_swap end");
	
	return frame_to_write;
}

void frame_free(size_t idx)
{
    printf("%s\n", "frame_free");

	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);
	bitmap_set(free_frames_bitmap, idx, FRAME_FREE);
    printf("%s\n", "frame_free end");

}


size_t rlu(){
	int64_t last_time = INT64_MAX;
	size_t idx = 0;
	for (int i = 0; i < no_of_pages; i++)
	{
		if (frame_table[i].spte != NULL)
		{
			if (frame_table[i].last_time < last_time)
			{
				last_time = frame_table[i].last_time;
				idx = i;
			}
		}
	}
	return idx;
} 
void set_last_accessed(int64_t last_time){
	for (int i = 0; i < no_of_pages; i++)
	{
		if (frame_table[i].spte != NULL)
		{
			if(pagedir_is_accessed(frame_table[i].ownner_thread->pagedir, frame_table[i].spte->virt_page_addr)){
				frame_table[i].last_time = last_time;
				pagedir_set_accessed(frame_table[i].ownner_thread->pagedir, frame_table[i].spte->virt_page_addr, false);
			}
		}
	}
}